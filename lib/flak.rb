require 'flak/version'

require 'yaml'
require 'json'
require 'rake/clean'
require 'find'
require 'fileutils'

require 'erb'
require "thor"
require "thor/group"
require "ap"
require 'nanoc'
require 'core_ext/string'
require 'core_ext/hash'

require 'flak/rake/file_actions'
require 'flak/rake/os'
require 'flak/rake/errors'
require 'flak/rake/target'

require 'flak/rake/templates/merge_engine'

require 'flak/rake/templates/environment'
require 'flak/rake/templates/maya'
require 'flak/rake/templates/nuke'
require 'flak/rake/templates/release'
require 'flak/rake/templates/doc'
require 'flak/rake/templates/maya_plugin'
require 'flak/rake/templates/maya_app'
require 'flak/rake/templates/shell'
require 'flak/rake/templates/gl'
require 'flak/rake/templates/delight'
require 'flak/rake/templates/cpp'


require 'flak/version'
require 'flak/thor/wizard'
require 'flak/thor/target_file'
require 'flak/thor/generate'
require 'flak/thor/cli'

