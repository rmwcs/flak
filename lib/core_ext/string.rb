class String

  # Find words, capitalize and join
  # @return [String] camel cased string
  def camelize
    self.split(/[^a-z0-9]/i).map{|w| w.capitalize}.join
  end



  # Find words breaks and insert underscores
  # @return [String] underscorized string
  def underscoreize
    self.gsub(/::/, '/').
    gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
    gsub(/([a-z\d])([A-Z])/,'\1_\2').
    tr("-", "_").
    downcase
  end




  # Remove the erb extension if there is one
  # @return [String] string with no erb extension
  def no_erb
    self.sub(/\.erb$/,'')
  end


  # Replace dollar-curly braces, with
  # the value of environment variable
  # @return [String] string with all environment variables replaced
  def substitute_env_vars
    str = String.new(self)
    str.scan(/\$\{([A-Z][A-Z,0-9,_]*)\}/).uniq.each do |m|
      begin
        str.gsub!("${#{m[0]}}", ENV[m[0]])
      rescue
        # $stderr.puts "problem with enviroment variable #{m[0]}."
        raise "Enviroment variable does not exist #{m[0]}."
      end
    end
    str
  end

end