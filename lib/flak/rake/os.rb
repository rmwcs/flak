module Flak
  # Provide a slightly nicer OS string than the 
  # RUBY_PLATFORM variable
  # @return [String] Either 'linux64', 'linux32', 'darwin64', or 'win64'
  def self.os
    case RUBY_PLATFORM 
    when /64-linux/ 
      'linux64'
    when /i686_linux/
      'linux32'
    when /darwin/
      'darwin64'
    when /i386-cygwin/
      'win64'
    end
  end

end