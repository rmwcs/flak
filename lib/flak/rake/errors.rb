# encoding: utf-8

module Flak

  # Module that contains flak error functions
  module Errors



    # Assert that a variable exists and has the correct type.
    # Prints a bright red informative message if not.
    # @param var [String] the variable's name
    # @param type [String] the variable's type
    # @param bind [Binding] the binding for the calling object so that the variable can be evaluated.
    # @param file [String] the filename where the call for this assertion came from.
    # @return [String] formatted error message.
    def self.assert(var, type, bind, file)
      param = eval(var,bind)
      ap "#{var} is not a #{type}. It is a #{param.class}. (#{File.basename(file)})" , :color => {:string => :red}  unless param.class == Kernel.const_get(type)
    end





  end

end
