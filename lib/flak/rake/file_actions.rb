module Flak

  module FileActions




    # in order to make a symlink, we need to remove it 
    # if it already exists


    # Create a symlink.
    # Remove it if it already exists.
    # @param target_file [String] the file to point the new file at.
    # @param new_file [String] the new file.
    def rebuild_symlink(target_file,new_file)
      if File.symlink?(new_file)
        File.unlink(new_file)
      end
      File.symlink(target_file, new_file)
    end




    # Remove a file and copy a new file over. 
    # If we just copy source to destination and destination exists and is a directory, 
    # then the src is put in the destination, as opposed to overwriting it. For
    # this reason, we delete destination first.
    # @param source [String] the source file.
    # @param destination [String] the destination file.
    def remove_and_copy(source,destination)
      remove_file(destination, true) 
      cp_r source, destination
    end




    # Make a directory to contain a file.
    # Create directories recursively if necessary.
    # @param file [String] the file that needs a directory.    
    def make_directory_for(file)
      FileUtils.mkdir_p file.pathmap('%d')
    end



    # Create a destination file by filtering a source file through ERB.
    # @param erb_file [String] the file to be filtered.    
    # @param released_file [String] the result filename.  
    # @param opts [Hash] the options.
    # @option opts :chmod [String] The file mode.
    def write_erb_template(erb_file,released_file, opts={})
      if (!(opts[:no_force] && File.exists?(released_file) ))
        template = ERB.new( File.read(erb_file) , 0, "%<>")
        File.open(released_file, 'w') do |f|
          puts "template #{erb_file} #{released_file}"
          f.puts(template.result(binding))
          f.chmod(opts[:chmod].to_i)   if opts[:chmod]
        end
      end
    end


  end



end
