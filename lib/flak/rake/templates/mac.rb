module Flak::Template
  module Mac

    module Settings

      extend Flak::Template::MergeEngine

      def self.extended target
        infuse target
      end

      def mac_app_release_path(file)     
        bind = binding()
        Flak::Errors.assert("@settings[:revision_directory]", "String", bind,__FILE__)
        Flak::Errors.assert("@settings[:product_revision]", "String", bind,__FILE__)
        File.join(@settings[:revision_directory], 'bin', "#{file.pathmap('%f').pathmap('%X')}-#{@settings[:product_revision]}#{file.pathmap('%x')}")   
      end

    end

    module Tasks

    end

  end

end


