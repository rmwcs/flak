module Flak::Template

  module Maya


    module Settings

      extend Flak::Template::MergeEngine

      def self.extended target
        infuse target
      end

      def self.settings_modifications settings

        mods = Hash.new

        bind = binding()
        Flak::Errors.assert("settings[:revision_directory]", "String", bind,__FILE__)
        mods[:maya_release_path] = File.join(  settings[:revision_directory],"maya" )

        mods
      end

      def outliner_icon_build_filename(file)
        icon_build_filename("out_#{file}")
      end

      def dg_icon_build_filename(file)
        icon_build_filename(file)
      end

      def icon_build_filename(file)
        bind = binding()
        Flak::Errors.assert("@settings[:build_directory]", "String", bind,__FILE__)
        File.join(@settings[:build_directory],'icons', file.pathmap('%f').ext('xpm') ) 
      end

    end


    module Tasks


      def self.extended target
        task_factory target
      end



      def self.generate_node_icon_tasks target
        settings = target.settings


        source_files = settings[:maya_node_icon_files]
        unless source_files.nil?

          source_files.each do |f|

            ['outliner','dg'].each do |type|

              build_file = target.send "#{type}_icon_build_filename", f

              dest_file = target.destination_filepath( settings[:maya_icon_destination], build_file)

              file build_file => f do |t|
                target.make_directory_for(build_file)
                cmd_key = "#{type}_convert_cmd".to_sym
                cmd = "#{settings[cmd_key]} #{f} #{build_file}"
                sh cmd
              end

              file dest_file => build_file do |t|
                target.make_directory_for(dest_file)
                cp build_file, dest_file 
              end         

              task :release => dest_file 
            end
          end
        end
      end






      def self.generate_icon_tasks target
        settings = target.settings


        source_files = settings[:maya_icon_files]
        unless source_files.nil?

          source_files.each do |f|

            build_file = target.icon_build_filename f

            dest_file = target.destination_filepath( settings[:maya_icon_destination], build_file)

            file build_file => f do |t|
              target.make_directory_for(build_file)
              cmd = "#{settings[:icon_convert_cmd]} #{f} #{build_file}"
              sh cmd
            end

            file dest_file => build_file do |t|
              target.make_directory_for(dest_file)
              cp build_file, dest_file 
            end         

            task :release => dest_file 

          end
        end
      end







      # generate tasks here
      ################################################################################
      def self.task_factory target
        settings = target.settings
        self.generate_node_icon_tasks target
        self.generate_icon_tasks target
      end


    end


  end


end
