module Flak::Template
  module Cpp

    module Settings

      extend Flak::Template::MergeEngine

      def self.extended target
        infuse target
      end




      # these methods will become instance methods of the target
      # and as such will have access to te @settings instance variable
      ###################################################
      def build_filename
        bind = binding()
        Flak::Errors.assert("@settings[:build_directory]", "String", bind,__FILE__)
        Flak::Errors.assert("@settings[:name]", "String", bind,__FILE__)
        Flak::Errors.assert("@settings[:target_extension]", "String", bind,__FILE__)
        File.join(@settings[:build_directory],'products',@settings[:name].ext(@settings[:target_extension])) 
      end

      def release_filename 
        bind = binding()
        Flak::Errors.assert("@settings[:revision_directory]", "String", bind,__FILE__)
        File.join( @settings[:revision_directory], 'bin', build_filename.pathmap('%f'))   
      end

      # generate path to object file from source: e.g. /hq/dev/jtools/build/2009.3/darwin/debug/animal/sensor.o"
      def object_file(source)
        bind = binding()
        Flak::Errors.assert("@settings[:build_directory]", "String", bind,__FILE__)
        Flak::Errors.assert("@settings[:object_extension]", "String", bind,__FILE__)
        File.join(@settings[:build_directory], source.gsub("/src/","/").ext( @settings[:object_extension] ))
      end

      # collect all object files for this target
      def object_files
        @settings[:source_files].collect { |s| object_file(s) }
      end

      def c_compile_cmd(source)
        bind = binding()
        Flak::Errors.assert("@settings[:compiler]", "String", bind,__FILE__)
        Flak::Errors.assert("@settings[:object_flag]", "String", bind,__FILE__)
        Flak::Errors.assert("@settings[:include_flag]", "String", bind,__FILE__)

        compiler = "\"#{@settings[:compiler]}\""
        object = "#{@settings[:object_flag]}\"#{object_file(source)}\""
        source = "\"#{source}\""
        inc_path = (@settings[:source_files].collect {|f| f.pathmap('%d')}.uniq | ( @settings[:include_paths] || [] )).collect { |el| "#{@settings[:include_flag]}\"#{el.to_s}\"" }.join(" ")
        includes = ( @settings[:includes] || [] ).collect { |el| "-include  \"#{el.to_s}\"" }.join(" ")
        compiler_options = (@settings[:compiler_options] || [] ).collect { |el| el.to_s }.join(" ")
        # puts "*" * 80
        # puts @settings[:compiler_options]
        # puts "*" * 80
        "#{compiler} #{compiler_options}  #{source} #{object}  #{inc_path} #{includes}"
      end


      def c_link_cmd
        bind = binding()
        Flak::Errors.assert("@settings[:linker]", "String", bind,__FILE__)
        Flak::Errors.assert("@settings[:lib_flag]", "String", bind,__FILE__)
        Flak::Errors.assert("@settings[:lib_ext]", "String", bind,__FILE__)
        Flak::Errors.assert("@settings[:libpath_flag]", "String", bind,__FILE__)
        Flak::Errors.assert("@settings[:framework_flag]", "String", bind,__FILE__)
        Flak::Errors.assert("@settings[:outputfile_flag]", "String", bind,__FILE__)


        linker = "\"#{@settings[:linker]}\""
        objects = object_files.collect { |el| "\"#{el.to_s}\"" }.join(" ")
        #  libstr =  ((@settings[:libs] || []).collect { |el| "-l#{el.to_s}" } |  (@settings[:static_libs] || []).collect { |el| "#{@settings[:static_prefix_flag]} -l#{el.to_s}" }).join(" ")
        libstr =  (@settings[:libs] || []).collect { |el| "#{@settings[:lib_flag]}#{el.to_s}#{@settings[:lib_ext]}" }.join(" ")
        libpathstr = (@settings[:lib_paths] || []).collect { |el| "#{@settings[:libpath_flag]}\"#{el.to_s}\"" }.join(" ")
        linkflagstr = ( @settings[:linker_options]  || [] ).collect { |el| el.to_s }.join(" ")
        dso_flagstr = (@settings[:dso_options] || [] ).collect { |el| el.to_s }.join(" ")
        fwrk = (@settings[:frameworks] || [] ).collect { |el| "#{@settings[:framework_flag]} #{el.to_s}" }.join(" ")
        "#{linker}  #{dso_flagstr} #{linkflagstr}  #{libpathstr} #{libstr} #{fwrk}   #{@settings[:outputfile_flag]}\"#{build_filename}\" #{objects}"
      end


      def c_clean_cmd
        object_str = object_files.collect { |el| "\"#{el.to_s}\"" }.join(" ")
        "rm -f \"#{build_filename}\" #{object_str}"
      end
      ###################################################




    end





    module Tasks
      def self.extended target
        task_factory target
      end


      def self.task_factory target
        settings = target.settings


        files = settings[:source_files]
        unless files.nil?




          # file tasks to compile objects from sources
          # make sure the object file depends on the 
          # source and header if it exists
          ######################################################    
          files.each do |f|
            object = target.object_file(f)  
            file object => f  do |t|
              target.make_directory_for(object)
              sh target.c_compile_cmd(f)
            end
            header = f.ext('h')
            file object => header if File.exists?(header) 
          end
          ######################################################     





          # file tasks to link targets
          ######################################################
          objects = target.object_files
          build_file = target.build_filename
          file build_file => objects do 
            target.make_directory_for(build_file)
            sh target.c_link_cmd
          end

          namespace settings[:name].to_sym do             
            desc "build the #{settings[:name].to_sym} binary"
            task :build => build_file # add named target
          end
          task :build => "#{settings[:name].to_sym}:build"
          ######################################################







          # tasks to clean the build
          ######################################################
          namespace settings[:name].to_sym do     
            desc "clean the #{settings[:name].to_sym} build files"
            task :clean do
              sh target.c_clean_cmd
            end
          end
          task :clean => "#{settings[:name].to_sym}:clean"
          ######################################################







          # plugins and executables release tasks
          ######################################################
          release_binary = target.release_filename
          build_file = target.build_filename  
          file release_binary => build_file do
            target.make_directory_for(release_binary)
            rm_r release_binary  if File.exists?(release_binary)
            cp build_file, release_binary 
            File.chmod 0755, release_binary 
          end
          ######################################################





          # add the release_binary file task to the tool's release task
          # and add the tool's release task to the release task
          # in the global namespace
          ######################################################
          namespace settings[:name].to_sym do             
            task :release => release_binary
          end

          task :release => "#{settings[:name].to_sym}:release"
          ######################################################

        end



      end

    end
  end
end

