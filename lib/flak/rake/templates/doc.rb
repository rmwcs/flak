module Flak::Template


  module Doc
    module Settings

      extend Flak::Template::MergeEngine

      def self.extended target
        infuse target
      end

      def self.settings_modifications settings

        mods = Hash.new

        bind = binding()
        Flak::Errors.assert("settings[:product_revision]", "String", bind,__FILE__)
        Flak::Errors.assert("settings[:release_root]", "String", bind,__FILE__)

        mods[:doc_directory_name] = "#{settings[:product_revision]}/doc"

        mods[:doc_destination] =  File.join(  settings[:release_root] , mods[:doc_directory_name] )

        mods
      end

      def doc_default_hash(item)
        bind = binding()
        Flak::Errors.assert("@settings[:product_name]", "String", bind,__FILE__)
        Flak::Errors.assert("@settings[:product_revision]", "String", bind,__FILE__)
        Flak::Errors.assert("@settings[:author_name]", "String", bind,__FILE__)
        Flak::Errors.assert("@settings[:author_email]", "String", bind,__FILE__)

        {
          :title => "#{@settings[:product_name].capitalize} #{item[:type]}", 
          :description=> "#{@settings[:product_name].capitalize} #{@settings[:product_revision]} #{item[:type]}",
          :author=> @settings[:author_name],
          :email=> @settings[:author_email],
          :created_at => Time.now.strftime("%d %B %Y")
        }
      end


      def doc_plug_metadata(site)
        site.items.each do |item| 
          unless item.binary?
            item[:type] =  "Index" if item.identifier == "/"
            item[:type] =  "Release Notes" if item.identifier == "/release_notes/"
            item.attributes= doc_default_hash(item).merge(item.attributes)   
          end
        end
      end

    end


    module Tasks

      def self.extended target
        task_factory target
      end



      def self.task_factory target
        settings = target.settings


        desc "Release documentation"     
        task :doc do
          docroot = File.join(settings[:root],'doc')
          Dir.chdir(docroot) do |d|
            site = Nanoc::Site.new(YAML.load_file('config.yaml'))
            site.config[:output_dir] =   settings[:doc_destination]  
            target.doc_plug_metadata(site)
            puts "Compiling documentation site in #{settings[:doc_destination]}"
            site.compile
            puts "Done."
          end
        end
      end


    end


  end


end

