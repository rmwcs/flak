module Flak::Template

  module MergeEngine

    def infuse target
      settings = target.settings
      flat = flatten_yaml settings
      settings = settings.flak_merge(flat)
      module_mods = settings_modifications(settings)
      target.settings = settings.flak_merge(module_mods)
    end

    
    # substutute environment variables in yaml file
    # and faltten out the os and configuration keys.
    def self.flatten_yaml_file(settings, file)
      if File.exists? file
        content = File.open(file, "rb").read.substitute_env_vars
        yml = YAML::load(content)  || {}
        yml.flak_flatten(settings[:configuration], settings[:os])   
      else
        {}
      end
    end 


    # open the yaml file associated with the current module and flatten it
    def flatten_yaml settings
      file = settings[:root] + '/config/'+self.name.split("::")[2].underscoreize+'.yml' 
      Flak::Template::MergeEngine.flatten_yaml_file(settings,file)
    end
    
    
    
    # fallback modifications method for #{self.name}
    def settings_modifications settings
      {}
    end
    



  end

end