module Flak::Template

  module Shell




    module Settings

      extend Flak::Template::MergeEngine

      def self.extended target
        infuse target
      end




      def shell_script_destination(file)     
        bind = binding()
        Flak::Errors.assert("@settings[:revision_directory]", "String", bind,__FILE__)

        File.join(@settings[:revision_directory], 'bin', file.pathmap('%f'))   
      end


    end




    module Tasks

   #   def self.extended target
   #     task_factory target
   #   end
   #
   #   def self.task_factory target
   #     settings = target.settings
   #
   #   end

    end




  end




end

