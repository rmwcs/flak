





module Flak::Template

  module MayaApp

    module Settings

      extend Flak::Template::MergeEngine

      def self.extended target
        infuse target
      end



      def self.settings_modifications settings
        mods = Hash.new

        bind = binding()
        Flak::Errors.assert("settings[:maya_relative_lib_path]", "String", bind,__FILE__)
        Flak::Errors.assert("settings[:maya_location]", "String", bind,__FILE__)
        Flak::Errors.assert("settings[:product_revision]", "String", bind,__FILE__)
        Flak::Errors.assert("settings[:author_name]", "String", bind,__FILE__)



       # lib_path =  File.join( settings[:maya_location], settings[:maya_relative_lib_path], {:key => 'lib_path'})
        lib_path =  File.join(settings[:maya_location], settings[:maya_relative_lib_path])  
        
        mods[:lib_paths] = [ lib_path ]

        case settings[:os]
        when /linux64/ 
          mods[:linker_options] =  [ "-Wl,-rpath,#{lib_path}" ]
          mods[:include_paths] = [  File.join( settings[:maya_location], "include" ) ]  
        when /darwin64/
          mods[:linker_options] =  [ "-Wl,-executable_path,#{lib_path}" ]
          mods[:include_paths] = [  File.join(settings[:maya_location],"devkit", "include" ) ]  
        when /win64/
          mods[:linker_options] = []
          mods[:include_paths] = [  File.join(settings[:maya_location], "include" ) ]  
        end

        mods[:compiler_options] =  [
          "-D\"PLUGIN_VERSION=\\\"#{settings[:product_revision]}\\\"\"",
          "-D\"PLUGIN_VENDOR=\\\"#{settings[:author_name].gsub(' ','')}\\\"\"",
          "-D\"MAYA_VERSION=\\\"#{settings[:maya_version]}\\\"\""
        ]

        mods
      end





      # these methods will become instance methods of the target
      # and as such will have access to the @settings instance variable
      ###################################################
      def release_filename 
        File.join( @settings[:revision_directory], 'bin', build_filename.pathmap('%f'))   
      end
      ###################################################




    end





    module Tasks

      # there are no special tasks for maya app
      # as the CPP build tasks will be used

      #  def self.extended target
      #    task_factory target
      #  end
      #
      #  
      #  def self.task_factory target
      #
      #  end

    end

  end
end

