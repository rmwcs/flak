module Flak::Template

  module Nuke

    module Settings

      extend Flak::Template::MergeEngine

      def self.extended target
        infuse target
      end



      def self.settings_modifications settings

        bind = binding()
        Flak::Errors.assert("settings[:os]", "String", bind,__FILE__)
        Flak::Errors.assert("settings[:revision_directory]", "String", bind,__FILE__)


        mods = Hash.new

        case settings[:os]
        when /linux64/ 
          mods[:nuke_location] = ""
        when /darwin64/
        Flak::Errors.assert("settings[:nuke_version]", "String", bind,__FILE__)

          mods[:nuke_location] = "/Applications/Nuke#{settings[:nuke_version]}-32/Nuke#{settings[:nuke_version]}.app/Contents"
        when /win64/
          mods[:nuke_location] = ""
        end



        msg = 'Problem creating mods[:nuke_release_path] from: settings[:revision_directory],"nuke"'
        mods[:nuke_release_path] = File.join(settings[:revision_directory],"nuke" ) 


        mods
      end


    end


    module Tasks



    end


  end




end

