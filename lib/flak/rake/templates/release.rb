module Flak::Template

  module Release






    module Settings

      extend Flak::Template::MergeEngine

      def self.extended target
        infuse target
      end


      def release_script_destination(file)     

        bind = binding()
        Flak::Errors.assert("@settings[:revision_directory]", "String", bind,__FILE__)

        File.join( @settings[:revision_directory], 'bin', file.pathmap('%f').no_erb )   
      end



      def tar_filename
        bind = binding()
        Flak::Errors.assert("@settings[:product_name]", "String", bind,__FILE__)
        Flak::Errors.assert("@settings[:product_revision]", "String", bind,__FILE__)
        Flak::Errors.assert("@settings[:platform_id]", "String", bind,__FILE__)
        
        "#{@settings[:product_name]}-#{@settings[:product_revision]}-#{@settings[:platform_id]}.tar.gz"

      end

      def file_to_tar
        bind = binding()
        Flak::Errors.assert("@settings[:product_name]", "String", bind,__FILE__)
        Flak::Errors.assert("@settings[:product_revision]", "String", bind,__FILE__)
        n = "#{@settings[:product_name]}/#{@settings[:product_revision]}"
      end

    end





    module Tasks

      def self.extended target
        task_factory target
      end

      def self.task_factory target
        settings = target.settings


        desc "Build everything"
        task :build 
        CLEAN.include( File.dirname(settings[:build_directory])  )
        CLOBBER.include( settings[:revision_directory]) 

        desc "Release everything"     
        task :default => ["release"]




        desc "Build and tar up product to tar.gz."     
        task :tar => :release do
          Dir.chdir( settings[:platform_directory] ) do |d|
            sh "tar cfz #{target.tar_filename} #{target.file_to_tar}"
          end
        end




      end

    end


  end





end

