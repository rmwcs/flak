module Flak::Template
  module Environment

    module Settings

      extend Flak::Template::MergeEngine

      def self.extended target
        infuse target
      end


      def self.settings_modifications settings

        mods = Hash.new

        bind = binding()

        Flak::Errors.assert("settings[:product_revision]", "String", bind,__FILE__)
        Flak::Errors.assert("settings[:platform_id]", "String", bind,__FILE__)
        Flak::Errors.assert("settings[:configuration]", "String", bind,__FILE__)
        Flak::Errors.assert("settings[:tools_directory]", "String", bind,__FILE__)
        Flak::Errors.assert("settings[:product_name]", "String", bind,__FILE__)

        mods[:build_directory] = File.join( "build", settings[:product_revision] , settings[:platform_id], settings[:configuration])

        mods[:platform_directory] = File.join(settings[:tools_directory], settings[:platform_id])
        
        mods[:release_root] = File.join(mods[:platform_directory], settings[:product_name])

        mods[:revision_directory] =  File.join( mods[:release_root] , settings[:product_revision] )

        mods
      end


      def destination_filepath(relative_path, file)
        bind = binding()
        Flak::Errors.assert("@settings[:revision_directory]", "String", bind,__FILE__)
        f = File.join(@settings[:revision_directory] , relative_path , file.pathmap('%f').no_erb).gsub('./','')
      end


    end


    module Tasks

      def self.extended target
        task_factory target
      end

      # We want to release erb files by binding them using ERB. 
      # To help identify erb files we define them as Filelists in
      # yaml files under the key <identifier>_erb_files 
      # e.g. shell_erb_files or maya_script_erb_files 
      # for the identifiers: shell, and maya_script respectively
      def self.generate_erb_tasks target
        settings = target.settings

        namespace settings[:name].to_sym do     

          erb_keys = settings.keys.find_all {|el|  ( el.to_s =~ /^.*_erb_files$/)}

          erb_keys.each do |k|
            identifier = k.to_s.gsub("_erb_files", "") #  shell or mel
            files_key = identifier.to_sym        # :shell or :mel
            destination_path_key = "#{identifier}_destination".to_sym
            relative_path_key = "#{identifier}_relative".to_sym



            write_erb_opts = {:chmod => 0755}

            files = settings[k]
            unless files.nil?
              files.each do |f|


                f_relative = ''
                if settings.has_key? relative_path_key
                  f_relative = File.dirname(f).gsub(settings[relative_path_key],'')
                end

                if settings.has_key? destination_path_key
                  rel_dest = File.join(settings[destination_path_key], f_relative)   
                  destination_file = target.destination_filepath( rel_dest, f)
                else
                  if target.respond_to?(destination_path_key)
                    destination_file = target.send destination_path_key, f
                  else
                    destination_file = target.destination_filepath( f_relative, f)
                  end
                end

                file destination_file => f do
                  target.make_directory_for(destination_file)
                  target.write_erb_template(f,destination_file,write_erb_opts) 
                end

                # desc  "bind and release ERB #{destination_file.pathmap('%f')}"
                task destination_file.pathmap('%f') => destination_file

                task :release => destination_file.pathmap('%f') 


              end
            end
          end
        end

      end



      def self.generate_copy_tasks target
        settings = target.settings

        namespace settings[:name].to_sym do     

          # copy_keys is an array of keys representing file types that should be copied
          # for example maya_script_copy_files
          # If so, there should be a proc called maya_script_release_path 
          # which is responsible for generating the release path
          # this smells a bit - not sure if it can be fixed - we will see
          ##############################################
          copy_keys = settings.keys.find_all {|el|  ( el.to_s =~ /^.*_copy_files$/)}

          copy_keys.each do |k|
            identifier = k.to_s.gsub("_copy_files", "")

            # destination_path_proc = "#{identifier}_destination".to_sym
            destination_path_key = "#{identifier}_destination".to_sym
            relative_path_key = "#{identifier}_relative".to_sym

            files = settings[k]

            unless files.nil?

              files.each do |f|

                # if the relative_path_key exists, it is the part of the path
                # to strip away (along with the filename) to give a destination directory
                # for the particular file. 
                # Its good for when you want to release folders of stuff and keep the
                # hierarchy in tact - such as python packages.
                # Example - see medusa sanity
                f_relative = ''
                if settings.has_key? relative_path_key
                  f_relative = File.dirname(f).gsub(settings[relative_path_key],'')
                end

                # puts settings[relative_path_key]
                # puts f_relative
                

                if settings.has_key? destination_path_key
                  rel_dest = File.join(settings[destination_path_key], f_relative)  

                  destination_file = target.destination_filepath(rel_dest, f)
                else

                  if target.respond_to?(destination_path_key)
                    destination_file = target.send destination_path_key, f
                  else
                    destination_file = target.destination_filepath( f_relative, f)
                  end
                end

                file destination_file => f do
                  target.make_directory_for(destination_file)
                  sh "cp -r #{f}  #{destination_file}" 
                  File.chmod 0755, destination_file
                end
                task :release => destination_file
              end
            end
          end
          ##############################################
        end


      end


      def self.generate_inspect_and_release_tasks target
        settings = target.settings

        namespace settings[:name].to_sym do     

          desc  "See resolved configuration settings for #{settings[:name].to_sym}"
          task :inspect do
            ap  settings
          end

          desc "Release #{settings[:name].to_sym}"
          task :release 

        end 
        task :release => "#{settings[:name].to_sym}:release"

      end

      def self.task_factory target
        self.generate_erb_tasks target
        self.generate_copy_tasks target
        self.generate_inspect_and_release_tasks target
      end


    end


  end





end

