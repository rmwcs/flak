module Flak::Template


  module Delight
    module Settings

      extend Flak::Template::MergeEngine

      def self.extended target
        infuse target
      end


      def self.settings_modifications settings
        mods = Hash.new

        bind = binding()

        Flak::Errors.assert("settings[:delight_location]", "String", bind,__FILE__)
        Flak::Errors.assert("settings[:delight_version]", "String", bind,__FILE__)
        Flak::Errors.assert("settings[:build_directory]", "String", bind,__FILE__)

        v_loc =  File.join( "#{settings[:delight_location]}-#{settings[:delight_version]}" )


        mods[:lib_paths] =             [  File.join( v_loc ,"lib")  ]

        mods[:include_paths] =         [ File.join( v_loc ,"include")]

        mods[:shader_compiler] =       File.join( v_loc, "bin","shaderdl")

        mods[:shader_build_directory] =      settings[:build_directory]

        mods[:libs] =                  ["3delight"]

        mods[:shader_include_paths] = [ File.join( v_loc, "maya","rsl")] 

        mods  
      end




      # all shader build files
      def shader_files()
        @settings[:sl_files].collect { |f| shader_file(f) }
      end


      # path to shader build: e.g. /hq/dev/jtools/build/2009.3/darwin/debug/goosebump.sdl
      def shader_file(source)
        bind = binding()
        Flak::Errors.assert("@settings[:shader_build_directory]", "String", bind,__FILE__)

        File.join(@settings[:shader_build_directory], source.gsub("/src/","/").ext('sdl'))
      end

      def shader_release_path(file)
        bind = binding()
        Flak::Errors.assert("@settings[:revision_directory]", "String", bind,__FILE__)

        File.join(@settings[:revision_directory] , 'delight','shader', file.pathmap('%f'))
      end

      def delight_shader_compile_cmd(dest,src)  
        bind = binding()
        Flak::Errors.assert("@settings[:shader_compiler]", "String", bind,__FILE__)

        include_path_string = (@settings[:sl_files].collect {|f| f.pathmap('%d')}.uniq | ( @settings[:shader_include_paths] || [] )).collect { |el| "-I#{el.to_s}" }.join(" ")
        shader_type_string = "-DSHADER_TYPE_#{src.pathmap('%d').pathmap('%f')}"
        "#{@settings[:shader_compiler]} #{shader_type_string}  #{include_path_string} -d #{dest} #{src}"
      end


      def delight_clean_cmd
        "rm -f  #{shader_files}"
      end




    end





    module Tasks
      def self.extended target
        task_factory target
      end


      def self.task_factory target
        settings = target.settings



        files = settings[:sl_files]
        unless files.nil?

          files.each do |f|



            # file tasks to build shader from sources
            ######################################################
            target_shader = target.shader_file(f)    
            file target_shader => f  do |t|
              dest = target.make_directory_for(target_shader)
              sh delight_shader_compile_cmd(dest,f) 
            end

            namespace settings[:name].to_sym do        
              desc "build the #{settings[:name].to_sym} delight shaader"
              task :build => target_shader
            end
            task :build => "#{settings[:name].to_sym}:build"
            ######################################################            



            # file tasks to release shaders
            ######################################################
            release_shader = target.shader_release_path(target_shader)
            file release_shader => target_shader do
              target.make_directory_for(release_shader)
              cp target_shader, release_shader 
            end

            namespace settings[:name].to_sym do        
              task :release => release_shader
            end
            task :release => "#{settings[:name].to_sym}:release"
            ######################################################       


          end


          # task to clean the build files
          ######################################################
          namespace settings[:name].to_sym do     
            desc "clean the #{settings[:name].to_sym} build files"
            task :clean do
              sh delight_clean_cmd
            end
          end
          task :clean => "#{settings[:name].to_sym}:clean"
          ######################################################






        end
      end

    end


  end



end

