# This is where we prepare a target. 
# The target will "get" settings and methods from modules specified in the target.yml file

module Flak

  class Target

    include Flak::FileActions

    attr_accessor :settings

    # Constructs a target object.
    #
    # @param root [String] the directory containing the Rakefile.
    # @param target_file [String] the root-relative file that specifies how to build this target. Either project.yml or tool.yml
    def initialize(root, target_file)


      env_file_content = File.open( root  + '/config/environment.yml').read.substitute_env_vars
      tmp_env = YAML::load(env_file_content)

      goal_file_content = File.open(target_file).read.substitute_env_vars
      tmp_goal = YAML::load(goal_file_content)

      
      @settings = Hash.new     
      @settings[:configuration] =  tmp_env['configuration']
      @settings[:templates] = tmp_goal['templates'] 
      @settings[:os] =  Flak.os
      @settings[:root] = root
      @settings[:target_file] =  target_file 
      @settings[:full_target_file] =  File.join(@settings[:root], target_file )
      @settings[:relative_goal_root] =  File.dirname(target_file) 
      @settings[:goal_root] = File.dirname( @settings[:full_target_file] ) 
      @settings[:templates] = (["environment"] | @settings[:templates])
      
    end


    # After the Target has been constructed, generate settings and tasks.
    def build
      generate_settings
      generate_tasks
    end


    # Extend this Target with settings and methods from templates declared in the target file 
    def generate_settings

      @settings[:templates].each do |template_name|
        mod = Flak::Template.const_get("#{template_name.camelize}")
        mod = mod.const_get("Settings")
        raise TypeError, "#{mod.inspect} must be a Module" unless mod.kind_of? Module
        self.extend( mod )
      end

      yml = Flak::Template::MergeEngine.flatten_yaml_file(@settings, @settings[:target_file])
      @settings = @settings.flak_merge(yml)
      expand_filelists
      
    end


    # Extend this Target with tasks from templates declared in the target file 
    def generate_tasks

      @settings[:templates].each do |template_name|
        mod = Flak::Template.const_get("#{template_name.camelize}")
        mod = mod.const_get("Tasks")
        raise TypeError, "#{mod.inspect} must be a Module" unless mod.kind_of? Module
        self.extend( mod )
      end

    end



    private

    # Expand FileList objects into arrays so that calling inspect on a target gives a more readable output.
    def expand_filelists
      @settings.each do |k,v|
        if k.to_s =~ /_files$|_paths$/
          unless v.nil?
            v = v.collect { |el| (el[0] == '/') || (el[1] ==":") ?  el : File.join(@settings[:relative_goal_root],el) }
            @settings[(k.to_sym)] =  k.to_s =~ /_files$/ ? FileList[v].to_a : v
          end
        end        
      end
    end

  end
end