
# opts[:maya_plugin_target] = false
# opts[:maya_app_target] = false

# opts[:nuke_target] = false
# opts[:standalone_target] = false

# opts[:delight_target] = false
# 




module Flak
  module TargetFile


    # Generate a tool.yml target file according to options given by the user, usually through a wizard.
    # @param name [String] name of the tool.    
    # @param options [Hash] the options.
    # @option options :maya_plugin_target [true, false] Whether this tool will be the target for a Maya plugin.
    # @option options :maya_app_target [true, false] Whether this tool will be the target for a Maya standalone application.
    # @option options :delight_target [true, false] Whether this tool will be the target for a 3delight DSO, or filter.
    # @option options :standalone_target [true, false] Whether this tool will be the target for a standalone application.
    # @option options :maya_scripts [true, false] Whether this tool will contain Maya MEL or Python scripts.
    # @option options :nuke_scripts [true, false] Whether this tool will contain Nuke scripts or gizmos.
    # @option options :shell_scripts [true, false] Whether this tool will contain shell scripts.
    def self.tool(name,options={})
      h = Hash.new
      maya_c_compile = true if options[:maya_plugin_target] || options[:maya_app_target] 
      c_compile = true if (maya_c_compile || options[:standalone_target] ) 
      
      
      h['name'] =  name.camelize
      
      h['maya_script_copy_files'] = ["maya/script/**/*.mel"] if options[:maya_scripts]
      h['maya_python_script_copy_files'] = ["maya/script/**/*.py","maya/script/**/*.pyc"] if options[:maya_scripts]
      h['maya_python_script_relative'] = "#{name}/maya/script" if options[:maya_scripts]
      h['maya_scripted_plugin_copy_files'] = ["maya/script/plugin/**/*.py","maya/script/plugin/**/*.pyc"] if options[:maya_scripts]

      h['maya_node_icon_files'] = ["maya/icons/node/*"] if options[:maya_scripts]
      h['maya_icon_files'] = ["maya/icons/*.jpg"] if options[:maya_scripts]
      h['maya_icon_copy_files'] = ["maya/icons/*.png","maya/icons/*.xbm","maya/icons/*.xpm"]  if options[:maya_scripts]
      
      h['nuke_script_copy_files'] =  ["nuke/python/*.py", "nuke/gizmo/*.gizmo", "nuke/scenes/*.nk"] if options[:nuke_scripts]


      h['include_paths'] = ["../../shared/src"] if c_compile 
      h['include_paths'] << "../../shared/src/maya" if maya_c_compile
      h['libs'] = ["OpenMayaAnim","OpenMayaUI","OpenMayaRender","OpenMayaFX"]  if maya_c_compile
      h['templates'] = []
      h['templates'] << "cpp" if c_compile
      h['templates'] << "maya" if options[:maya_scripts]
      h['templates'] << "maya_plugin" if options[:maya_plugin_target]
      h['templates'] << "maya_app" if  options[:maya_app_target] 
      h['templates'] << "gl" if maya_c_compile
      h['templates'] << "delight" if options[:delight_target]
      h['templates'] << "nuke" if options[:nuke_scripts]
      h['templates'] << "shell"  if options[:shell_scripts]
      
      h['source_files'] = []
      h['source_files'] << "src/*.cpp" if c_compile
      h['shell_script_copy_files'] = ["shell/script/*"]  if options[:shell_scripts]

      h['python_package_copy_files'] = ["python_package/#{name}/*"] if options[:python_package]
      h['python_package_destination'] = "python/#{name}/" if options[:python_package]

      YAML::dump(h)
    end

    # Generate a project.yml target file according to options given by the user, usually through a wizard.
    # @param name [String] name of the project.    
    # @param options [Hash] the options.
    # @option options :maya_module [true, false] Whether this project will build a Maya module. 
    def self.project(name,options={})
      h = Hash.new
      h['name'] = "#{name.camelize}Project"
      h['templates'] = ["release", "doc"]
      
      h['os_darwin64'] = {}
      h['os_linux64'] = {}
      h['os_win64'] = {}

      h['os_darwin64']['release_script_copy_files'] =  ["resource/config/*.sh"] 
      h['os_linux64']['release_script_copy_files'] =  ["resource/config/*.sh"] 
      h['os_win64']['release_script_copy_files'] =  ["resource/config/*.BAT"] 


      if options[:maya_module]
        h['maya_script_copy_files'] = ["script/maya/*.mel","script/maya/*.py","script/maya/*.pyc"]
        h['maya_python_script_copy_files'] = ["script/maya/**/*.py"] 
        h['maya_python_script_relative'] = "script/maya" 

        h['maya_script_erb_files'] = ["script/maya/erb/*.mel.erb","script/maya/erb/*.py.erb"]
        h['templates'] << "maya"
        h['os_darwin64']['maya_modules_erb_files'] =  ["resource/maya/module*"] 
        h['os_linux64']['maya_modules_erb_files'] = ["resource/maya/module*"] 
      end
      
      h['default_copy_files'] = ["LICENSE.*", "README.*"] 
      h['package_erb_files'] = ["resource/raia/*"] 


      YAML::dump(h)
    end

    def self.raia_package(name,options={})
      h = Hash.new
      h['name'] = name
      h['description'] = "Description goes here"
      h['version'] = "<%= settings[:product_revision] %>"
      h['documentation'] = {"html" => "<%= settings[:revision_directory] %>/doc/index.html"}
      h['platform'] =  {"id" => "<%= settings[:platform_id] %>"}
      h['software_location'] =  "<%= settings[:revision_directory] %>"


      h['requirements'] = []
      if options[:maya_module]
        h['requirements'] << {"name" => "maya", "version" => '==2012'}
        h['requirements'] << {"name" => "jlib", "version" => '>=0.0.7'}
      end

      h['runtimes'] = []

      h['env'] ={}

      h['env']["#{name.upcase}_PROJECT_PATH"] = "$software_location"

      h['env']["MAYA_MODULE_PATH"] = "$env.MAYA_MODULE_PATH:$software_location/modules"  if options[:maya_module]

      h['env']["NUKE_PATH"] = "$env.NUKE_PATH:$software_location/nuke/scripts" if options[:nuke]

      h['env']["PYTHONPATH"] = "$env.PYTHONPATH:$software_location/python" if options[:python]

      if options[:delight]
        h['env']["DL_SHADERS_PATH"] = "$env.DL_SHADERS_PATH:$software_location/delight/shader" 
        h['env']["DL_PROCEDURALS_PATH"] = "$env.DL_PROCEDURALS_PATH:$software_location/delight/procedural" 
      end
      #JSON.pretty_generate(h)
      YAML.dump(h)
      

    end

  end
end
