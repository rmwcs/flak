
module Flak
  
  
  # Registers generate as a flak task
  class CLI < Thor
    map "g" => :generate    
    register(Generate, 'generate', 'generate <something>', 'Type flak generate for more help.')
  end
  
  
end