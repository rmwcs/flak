module Flak::Template

  # This example illustrates how to make a plugin template for Flak.
  # This is a rather complex example, intended to show all parts of the plugin.
  # It has a 2 step build->release process. See other template modules in flak for 
  # more and less complex examples.
  #
  # The module <%=name.camelize%> contains 2 modules: Settings and Tasks
  # The settings hash and any instance methods are managed in the Settings module
  # Rake Tasks are built in the Tasks module.
  # Both modules must exist in a plugin
  #
  # This plugin example relies on some settings to be declared in <%=name%>.yml and it needs files to work on.
  # To test the plugin as is you'll need to add the following lines to tool.yml 
  # for the tool that will use this plugin. (without '#'s of course)
  # <%=name%>_include_paths:
  # - '<%=name%>_src/inc/*.*'
  # <%=name%>_src_files:
  # - '<%=name%>_src/geo/*.*'
  # - '<%=name%>_src/light/*.*'
  # - '<%=name%>_src/shader/*.*'
  #
  # - and make some files that fit the above file globs.
  #
  # Also, in the same file, add - '<%=name%>' to the list of templates
  #
  # You'll notice you also have <%=name%>.yml in the config directory now
  module <%=name.camelize%>


    module Settings

      extend Flak::Template::MergeEngine

      # At the time the Target object extends with this plugin's Settings module, we call {Flak::Template::MergeEngine#infuse}
      # which takes care of reading the associated <%=name%>.yml if it exists, flattening it, merging it
      # with the modifications made below, and finally merging the whole lot into the Target.
      # @param target [Target] the object that will receive settings from this module
      def self.extended target
        infuse target
      end

      # Create new keys for the settings hash programatically.
      # The target's settings hash is available to use to make new values. For example, building new paths from components.
      # This method is called automatically by the infuse method above. You only need to be concerned with
      # creating the new keys and values you want, and returning them. Merging into target's settings is taken care of.
      # @param settings [Hash] the target's settings hash.
      def self.settings_modifications settings
        mods = Hash.new

        bind = binding()
        Flak::Errors.assert("settings[:<%=name%>_location]", "String", bind,__FILE__)
        Flak::Errors.assert("settings[:<%=name%>_version]", "String", bind,__FILE__)
        Flak::Errors.assert("settings[:build_directory]", "String", bind,__FILE__)

        <%=name%>_version_location =  File.join( "#{settings[:<%=name%>_location]}-#{settings[:<%=name%>_version]}" )

        mods[:<%=name%>_compiler] =       File.join( <%=name%>_version_location, "bin","fcompile")

        mods[:<%=name%>_build_directory] =      settings[:build_directory]

        mods[:libs] =                  ["<%=name.camelize%>"]

        mods[:<%=name%>_include_paths] = [ File.join( <%=name%>_version_location, "extra","finc") ] 

        mods  
      end




      # Generate names for output build files from the list of source files
      # @return [Array] filenames
      def <%=name%>_out_files()
        @settings[:<%=name%>_src_files].collect { |f| <%=name%>_out_file(f) }
      end

      # Generate a name for an output build file.
      # As we need @settings[:<%=name%>_build_directory], we test its existence first.
      # We pull the original filepath apart and rebuild the way we want. In this case
      # we turn /toolname/geo/name.ext into geo/name_geo.flib
      # 
      # @param source [String] source file
      # @return [String] destination file
      def <%=name%>_out_file(source)
        bind = binding()
        Flak::Errors.assert("@settings[:<%=name%>_build_directory]", "String", bind,__FILE__)

        type = source.pathmap('%d').pathmap('%f')
        dirname = source.pathmap('%d')
        basename = source.pathmap('%f')
        File.join(@settings[:<%=name%>_build_directory], dirname, "#{basename.pathmap('%X')}_#{type}.flib")
      end



      # As this is an example, there is no such thing as a <%=name%> compiler and we can't compile with it. 
      # So, to show how it would work, the fictitious compile command will be written into each file.
      # The actual command we issue is echo with a redirect into the destination.
      # 
      # @param src [String] the source filepath
      # @param dest [String] the destination filepath
      # @return [String] the full compile command
      def <%=name%>_compile_cmd(src, dest)  
        bind = binding()
        Flak::Errors.assert("@settings[:<%=name%>_compiler]", "String", bind,__FILE__)

        include_path_string = (@settings[:<%=name%>_src_files].collect {|f| f.pathmap('%d')}.uniq | ( @settings[:<%=name%>_include_paths] || [] )).collect { |el| "-I#{el.to_s}" }.join(" ")
        <%=name%>_type_string = "-DFOO_TYPE_#{src.pathmap('%d').pathmap('%f').upcase}"
        "echo The compile cmd is: #{@settings[:<%=name%>_compiler]}  #{<%=name%>_type_string}  #{include_path_string} -d #{dest} #{src} > #{dest}" 
      end

      # list of files to add to clean task
      def <%=name%>_clean_cmd
        "rm -f  #{<%=name%>_out_files}"
      end




    end




    module Tasks

      # when the target is extended with the <%=name.camelize%> Tasks module, the task_factory is called and all tasks are made.
      def self.extended target
        task_factory target
      end

      # task_factory creates Rake tasks. These tasks are built dynamically based on the list of
      # sources. There are 2 types of rake tasks. tasks, and file tasks.
      # tasks start with the task keyword. 
      # file tasks start with the file keyword.
      # see {http://martinfowler.com/articles/rake.html Martin Fowler's Rake page} for an explanation
      # of Rake tasks. 
      def self.task_factory target
        settings = target.settings

        files = settings[:<%=name%>_src_files]
        unless files.nil?

          files.each do |f|


            # create file tasks to compile out_files from sources
            out_file = target.<%=name%>_out_file(f)    
            file out_file => f  do |t|
              target.make_directory_for(out_file)
              sh target.<%=name%>_compile_cmd(f,out_file) 
            end


            # add the out_file task to the ToolName:build named task
            namespace settings[:name].to_sym do        
              task :build => out_file
            end




            # create file tasks to release out_files to release destination
            release_file = target.destination_filepath(settings[:<%=name%>_destination],out_file)
            file release_file => out_file do
              target.make_directory_for(release_file)
              cp out_file, release_file 
            end


            # add the release_file task to the ToolName:release named task
            namespace settings[:name].to_sym do        
              task :release => release_file
            end


            # add the ToolName:release named task to the top level :release named task         
            task :release => "#{settings[:name].to_sym}:release"

          end


          # task to clean the build files for ToolName
          namespace settings[:name].to_sym do     
            desc "clean the #{settings[:name].to_sym} build files"
            task :clean do
              sh target.<%=name%>_clean_cmd
            end


          end


          # add the ToolName:clean named task to the top level :clean named task     
          task :clean => "#{settings[:name].to_sym}:clean"


          namespace settings[:name].to_sym do      
            desc "build the #{settings[:name].to_sym} <%=name%> resource"  
            task :build 
          end


          # add the ToolName:build named task to the top level :build named task
          task :build => "#{settings[:name].to_sym}:build"

        end



      end

    end

  end

end

