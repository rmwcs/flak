module Flak



  # Thor tasks that generate files
  class Generate < Thor

    include Thor::Actions

    attr_accessor :name, :tool_options, :project_options
    
    # Provide source_root so that templates can be found
    def self.source_root
      File.join(File.dirname(__FILE__), "templates")
    end


    no_tasks do
      
      # Get project options from a wizard and cache them
      def project_options
        @project_options ||= Flak::Wizard.project_options
      end
      
      # Get tool options from a wizard and cache them
      def tool_options
        @tool_options ||= Flak::Wizard.tool_options
      end
    end


    desc "project [name]", "Create a project"
    map "p" => :project    
    # The project subcommand.  Called with: flak generate project <name>.
    # Creates a flak project and populates it with files as specified by the users answers to the project wizard.
    def project(name)
      empty_directory(name) 
      inside name do  
        invoke :config
      end
    end


    desc "config [name]", "Create config files in a project"
    map "c" => :config    
    # The config subcommand.  Called with: flak generate config <name>.
    # This is the same as the project subcommand, except it works from inside an existing project.
    def config(name)
      opts = self.project_options
      self.name = name
      empty_directory("build") 
      empty_directory("plugins")
      directory("config")
      directory("doc")
      template( "LICENSE.tt", "LICENSE.txt")
      template( "README.tt", "README.txt")
      template( "product.mel.tt" ,    "script/maya/erb/#{name}.mel.erb" ) if opts[:maya_module]
      copy_file("module.tt"  ,      "resource/maya/module.erb" ) if opts[:maya_module]
      
      copy_file( "INSTALL.sh.tt",   "resource/config/INSTALL.sh")
      template( "INSTALL.BAT.tt",   "resource/config/INSTALL.BAT")
      template(  "env.sh.tt"  ,   "resource/config/env.sh"  )
      
      create_file "resource/raia/package.yml" do
         Flak::TargetFile.raia_package(name,opts)
      end

      create_file  "project.yml" do 
        Flak::TargetFile.project(name,opts)
      end

      # rakefile
      copy_file  "Rakefile.tt",  "Rakefile.rb"
      copy_file  "gitignore.tt",  ".gitignore"
    end
    



    desc "tool [name]", "Create a tool"
    map "t" => :tool    
    # The tool subcommand.  Called with: flak generate tool <name>.
    # Creates a tool inside a flak project and populates it with files as specified by the users answers to the tool wizard.
    def tool(name)
      opts = self.tool_options
      self.name= name
      create_file  "#{name}/tool.yml" do 
        Flak::TargetFile.tool(name,opts)
      end
       create_common_files
       create_maya_script_tree  if opts[:maya_scripts]
       create_nuke_script_tree if opts[:nuke_scripts]
       create_shell_script_tree  if opts[:shell_scripts]
       create_python_tree  if opts[:python_package]
       create_maya_plugin_files if opts[:maya_plugin_target]
       create_app_files if opts[:maya_app_target] || opts[:standalone_target] 
       create_delight_tools_tree if opts[:delight_target]

    end

    no_tasks do

      #desc "create_maya_plugin_files [name]", "Makes files needed for Maya plugin development"
      def create_maya_plugin_files
        create_app_files
        template("maya_plugin_cpp.tt", "#{name}/src/plugin.cpp")
      end


      #desc "maya_app_tool [name]", "Makes tool files needed for Maya app development"
      def create_app_files
        template("name_cpp.tt", "#{name}/src/#{name}.cpp")
        template("name_h.tt", "#{name}/src/#{name}.h"  )
      end




      #desc "create_delight_tree [name]", "Makes tool files needed for delight development"
      def create_delight_tools_tree
        empty_directory("#{name}/delight/shader/include") 
        empty_directory("#{name}/delight/shader/light") 
        empty_directory("#{name}/delight/shader/surface") 
        empty_directory("#{name}/delight/shader/displacement") 
        empty_directory("#{name}/delight/shader/atmosphere") 
        empty_directory("#{name}/delight/shader/interior") 
        empty_directory("#{name}/delight/dso") 
        empty_directory("#{name}/delight/filter") 
      end

      #desc "create_common_files [name]", "Makes files that will be in every tool"
      def create_common_files
        empty_directory("#{name}/examples") 
        create_file "#{name}/README"
        #  create_file "#{name}/LICENSE"
        # create_file "INSTALL.md"
      end
    
      #desc "create_maya_script_tree [name]", "Makes tool files needed for Maya script development"
      def create_maya_script_tree
      #  self.name= name
        empty_directory("#{name}/maya/script/plugin")
        empty_directory("#{name}/maya/icons/node") 
        template("init.mel.tt", "#{name}/maya/script/#{name}.init.mel")
        copy_file(  "workspace.mel"  ,     "#{name}/examples/workspace.mel"               )

      end

      #desc "create_nuke_script_tree [name]", "Makes tool files needed for nuke development"
      def create_nuke_script_tree
        empty_directory("#{name}/nuke/gizmo") 
        empty_directory("#{name}/nuke/python") 
        empty_directory("#{name}/nuke/scenes")
      end

      #desc "create_shell_script_tree [name]", "Makes tool files needed for shell script development"
      def create_shell_script_tree
        empty_directory("#{name}/shell/script") 
      end

            #desc "create_shell_script_tree [name]", "Makes tool files needed for shell script development"
      def create_python_tree
          template("__init__.py.tt", "#{name}/python_package/#{name}/__init__.py")
      end
    end




    desc "plugin [name]", "Create a plugin"
    map "u" => :plugin    
    # The plugin subcommand.  Called with: flak generate plugin <name>.
    # Creates an example plugin with the given name inside a flak project.
    def plugin(name)
      self.name= name
       template("plugin.yml.tt", "config/#{name}.yml")
       template("plugin.rb.tt", "plugins/#{name}.rb")
       
       say("The plugin was created in plugins/#{name}.rb and a config file was made in config/#{name}.yml")
       say("To use this plugin, edit tool.yml") 
       say("Add '#{name}' to the templates section, and add files needed by this plugin to a file list")
       say("See comments in plugins/#{name}.rb for more details")

    end

    desc "raia", "Create a raia package file"
    map "r" => :raia    
    def raia
      name=  File.basename(destination_root)
      opts = {}
      opts[:maya_module] = true
      create_file 'resource/raia/package.yml' do
         Flak::TargetFile.raia_package(name,opts)
      end
    end

    #############
    # Make Config Files individually
    desc "templates", "Create config templates. Run this from an existing project directory"
    map "m" => :templates    
    def templates
      self.name=   File.basename(destination_root)
      directory("config")
    end
    
    
    #############



    # Format the help line so it reads flak generate something name,
    # as opposed to flak generate:something name.
    def self.banner(task, namespace = false, subcommand = true)
      task.formatted_usage(self, true, subcommand).split(':').join(' ')
    end

  end

end






