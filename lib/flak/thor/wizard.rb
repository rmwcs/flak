module Flak
  module Wizard

    # Promt the user for information about the project to be created.
    # The result values are used to decide what files to generate and in some cases
    # to define the content of files such as project.yml.
    # @return [Hash] true or false values indicating the type of project.
    def self.project_options
      asker = Thor::Shell::Basic.new
      opts = Hash.new
      
      #asker.say("Flak can set up environment variables for this project based on the tools it contains. So please answer the following questions:") 
      
      opts[:maya_module] = asker.yes?("Will this project contain a Maya module? (y/n)")
      
      opts[:delight] = asker.yes?("Will this project contain 3delight tools? (y/n)")
      
      opts[:nuke] = asker.yes?("Will this project contain nuke tools? (y/n)")
      
      opts[:python] = asker.yes?("Will this project contain python modules? (y/n)")
      
      opts
    end


    # Promt the user for information about the tool to be created.
    # The result values are used to decide what files to generate and in some cases
    # to define the content of files such as tool.yml.
    # @return [Hash] true or false values indicating the type of tool.
    def self.tool_options
      asker = Thor::Shell::Basic.new
      opts = Hash.new

      opts[:maya_plugin_target] = false
      opts[:maya_app_target] = false
      opts[:delight_target] = false
      opts[:standalone_target] = false
      opts[:maya_scripts] = false
      opts[:nuke_scripts] = false
      opts[:shell_scripts] = false
      opts[:python] = false

      finished_target = false
      
      finished_target = opts[:maya_plugin_target] = opts[:maya_scripts] = asker.yes?("Is this tool a Maya plugin? (y/n)") unless finished_target

      finished_target = opts[:maya_app_target] = opts[:maya_scripts] =  asker.yes?("Is this tool a Maya standalone application? (y/n)") unless finished_target

      finished_target = opts[:delight_target] = asker.yes?("Will this tool contain 3delight shaders filters or DSOs? (y/n)")  unless finished_target

      finished_target = opts[:standalone_target] = asker.yes?("Will this tool contain a standalone application? (y/n)") unless finished_target

      opts[:maya_scripts] = asker.yes?("Will this tool also contain maya scripts? (y/n)")  unless opts[:maya_scripts]
  
      opts[:nuke_scripts] = asker.yes?("Will this tool also contain nuke scripts? (y/n)") 
      
      opts[:shell_scripts] = asker.yes?("Will this tool also contain shell scripts? (y/n)") 

      opts[:python_package] = asker.yes?("Will this tool also contain a python package? (y/n)") 

      opts
    end

  end
end
