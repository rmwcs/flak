

Flak
======

Description
--------

Flak is a framework to help you write VFX tools.

You will find flak especially useful if you write tools for Maya but it also helps you write shell scripts, Nuke tools, 3delight shaders and more. 


With flak, you create a project which contains any number of tools. Flak projects use Rake (Ruby Make), and as such, one set of configuration files work across all platforms. No need to fire up Visual Studio on Windows or xcode on Mac.

Flak also encourages good documentation by setting up a website skeleton based on [nanoc](http://http://nanoc.stoneship.org/) There are templates for most pages you will need for a software project, so you can concentrate on content and let flak handle presentation at compile time. In addition there are Maya tools to automatically generate flak documentation pages for plugin nodes, commands and scripts.

Install Flak
--------

Update rubyGems:

	$ sudo gem update --system

Install flak

    $ sudo gem install flak

Usage
--------

Flak currently has the task: generate, which has two subcommands: project and tool.

Tasks:

    flak generate help [COMMAND]  # Describe subcommands or one specific subcommand
    flak generate project [name]  # Create a project
    flak generate tool [name]     # Create a tool

Tasks and subcommands have a single letter alias. Use g for generate, p for project, t for tool.

Create a project
--------

For this guide we'll create a project called flintstone containing 2 tools, a maya plugin called fred and a compiled shell command called wilma. Generate a project with the following command.

    flak generate project flintstone

or

    flak g p flintstone

It will ask a few questions about the kind of module you want to make. This is so that the relevant paths can be set when the module is installed. Answer yes to all questions for now.

cd into flintstone, fire up your favorite editor and have a look at what was created.

* project.yml - Contains settings related to the project, but not including any of the tools it will contain. 
* Rakefile.rb - You never need to touch this.
* build/ - products will be built here before being released
* config/ - configuration files for different types of build
* script/ - project related set-up scripts
* doc/ - documentation

We'll look at the contents of the config directory and project.yml in more depth later. For now, open config/environment.yml and enter your full name in the author field.

As mentioned, Rake is the tool that will build your project. Some project related tasks are available even though the project contains no tools yet. Type rake -T to see a list of named rake tasks.

    rake FlintstoneProject:inspect             # See resolved configuration settings for FlintstoneProject
    rake FlintstoneProject:release             # Release FlintstoneProject
    rake build                                 # Build everything
    rake clean                                 # Remove any temporary products.
    rake clobber                               # Remove any generated file.
    rake doc                                   # Release documentation
    rake doc:tar                               # Build and tar up documentation to tar.gz.
    rake tar                                   # Build and tar up product to tar.gz.

To see the full hash of project settings, type:  

    rake FlintstoneProject:inspect

These settings are resolved for the configuration and operating system you are using.

To build the project type:

    rake

There should now be the folder  ~/tools/flintstone/0.0.1-darwin with bin and maya subdirectories. There's an install script and some other stuff that will become clear later. 

Back in the flintstone shell, type:

    rake tar

It will tar up the project in your tools folder ready to be deployed to another site.

Now type:

    rake doc

You'll see a website has been made in ~/tools/flintstone/0.0.1-doc The source files for this website are in the project's doc directory in the textile markup format. The templates are designed to allow you to concentrate on writing content without worrying about presentation. When you want to tar up the documentation site, type:

    rake doc:tar

Create tools
--------

Now generate some tools. Make sure you are in the flintstone directory and enter: 

    flak generate tool fred

Enter "y" for maya plugin. A tool can have at most one compiled target, which is given the name specified in the "name" field in the file tool.yml. It can however have any number of supporting scripts, icons or other resources. Its also fine to have no compiled tools and only create scripts. If that's the case, you can edit config/environment.yml and change agnostic to true. 

As fred is a maya plugin, it will automatically create maya scripts directories. It will also ask if the tool will contain nuke and shell scripts, and if so it will create directories to hold those scripts. Answer "y" for now, just to see what happens. When done, you'll see many new files, including cpp and headers in the src directory.

Now generate the tool wilma, but this time say no to maya plugin and other options until you are asked if it will be a standalone app. enter y.

    flak g t wilma

At this point, you have a skeleton project set up with tools. Now build it again by typing:

    rake

At this stage you may find the compilation fails. If so, check the output in the shell. Maybe paths to compilers are wrong. You can fix these and other settings in config/cpp.yml, config/maya.yml, config/maya_plugin.yml etc.

NOTE: When you type rake with no arguments, you are in fact running the default rake task, which is to release all tools, but no documentation.

You may also notice that since you made the fred and wilma tools there are more rake tasks available when you type:

    rake -T

If there was a problem building Wilma, you may be able to track it down by checking settings for Wilma:

    rake Wilma:inspect  

When the build succeeds, you will find there is now a standalone tool ~/tools/flintstone/0.0.1-darwin/bin/Wilma and a maya plugin ~/tools/flintstone/0.0.1-darwin/maya/plug-ins/Fred with a platform dependent extension: (.bundle on mac, .so on linux, .mll or windows). Of course, these compiled tools don't do anything and the plugin won't load, but at least you have a build process working. If you make a change to one of the source files, wilma.h for example, and type `rake` again you will see that only Wilma gets rebuilt.

Install your module
--------

####Mac or Linux####

On Mac or Linux, you can now install the module in your environment by running the install script: 

    ~/tools/flintstone/0.0.1-darwin/bin/INSTALL_FLINTSTONE

The purpose of this script is to set up your environment. 

As flintstone is a Maya module, the file in $MAYA_APP_DIR/$MAYA_VERSION/modules/flintstone will be written and will contain the following line:

    + flintstone 0.0.1 /Users/julian/tools/flintstone/0.0.1-darwin/maya

which specifies the path to the module. On startup, Maya appends the suffixes “plug-ins”, “presets”, “scripts”, and “icons”, to this path, and then adds the appended path to `MAYA_PLUG_IN_PATH`, `MAYA_PRESET_PATH`, `MAYA_SCRIPT_PATH`, and `XBMLANGPATH`, respectively. Read more about Maya modules [here](http://download.autodesk.com/us/maya/2010help/index.html?url=Environment_Variables_File_path_variables.htm,topicNumber=d0e676929)

In addition to the maya module setup, Flak also creates a file that appends some locations in the flintstone module to the system path variables and other relevant paths. Have a look in the file: 

~/tools/flintstone/0.0.1-darwin/bin/flintstone.sh

This file is sourced from your .bashrc (have a look), so the variables it contains are available in your shell sessions.

####Windows####

For Windows, the process of installing a released flak module for the user is somewhat more manual. A more automated solution is in progress.

If the module is a Maya module, then make the folder: C:\Users\my_name\Documents\maya\2012-x64\modules and create a plain text document called flintstone. Its important to make sure it has no extension. flintstone.txt for example will NOT work.

Add the following line, 

    flintstone 0.0.1 <path/to/tools>/flintstone/0.0.1-win_64/maya

If the module contains shell scripts, nuke scripts or other files that rely on environment variables, you'll have to append the relevant paths to the system variables. For example:

    FLINTSTONE = <path/to/tools>\flintstone\0.0.1-win_64
    PATH =   		%PATH%;%FLINTSTONE%\bin
    NUKE_PATH =   	%NUKE_PATH%;%FLINTSTONE%\nuke\scripts
