# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "flak/version"

Gem::Specification.new do |s|
  s.name        = "flak"
  s.version     = Flak::VERSION
  s.authors     = ["Julian Mann"]
  s.email       = ["julian.mann@gmail.com"]
  s.homepage    = "https://bitbucket.org/rmwcs/flak"
  s.summary     = %q{build system for VFX tools}
  s.description = %q{VFX tool build and documentation framework based on rake with thor generators}
  s.add_dependency('thor')
  s.rubyforge_project = "flak"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  s.add_runtime_dependency "awesome_print"
  s.add_runtime_dependency "nanoc"
  s.add_runtime_dependency "sass"
  
  s.add_development_dependency "RedCloth"
  s.add_development_dependency "yard", "= 0.7.2"
  s.add_development_dependency "redcarpet"
  
  s.has_rdoc = 'yard'

end
